package utils

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"

	"github.com/go-redis/redis"
	"gopkg.in/olivere/elastic.v3"
)

//Config define config
var Config config

type config struct {
	Log   *log.Logger
	ES    *elastic.Client
	Redis *redis.Client
}

//Log return logger
func Log() *log.Logger {
	return Config.Log
}

//Init utilsl
func Init() {
	initLog()
	initConfig()
	initElastic()
}

func initElastic() {
	Config.ES.CreateIndex("torrent").Do()
}

func initConfig() {
	type config struct {
		Database string `json:"database"`
		Elastic  string `json:"elastic"`
	}

	f, err := os.Open("config/crawl.conf")
	exit(err)
	b, err := ioutil.ReadAll(f)
	exit(err)
	var c config
	err = json.Unmarshal(b, &c)
	exit(err)

	client, err := elastic.NewClient(elastic.SetURL(c.Elastic))
	exit(err)
	Config.ES = client

	redisClient := redis.NewClient(&redis.Options{
		Addr:     c.Database,
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	Config.Redis = redisClient
}

func initLog() {
	Config.Log = log.New(os.Stdout, "", log.Ldate|log.Ltime|log.Lshortfile)
}

func exit(err error) {
	if err != nil {
		Log().Fatalln(err)
	}
}
