package parser

import (
	"bytes"
	"encoding/base32"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/btlike/storage/utils"
)

//define address
const (
	Xunlei  = "http://bt.box.n0808.com/%s/%s/%s.torrent"
	Vuze    = "http://magnet.vuze.com/magnetLookup?hash=%s"
	Torrage = "http://178.73.198.210/torrent/%s.torrent"
)

//define errors
var (
	ErrNotFound = errors.New("not found")
)

//DownloadXunlei torrent
func DownloadXunlei(hash string, client *http.Client) (mi MetaInfo, err error) {
	mi.InfoHash = hash
	if len(hash) != 40 {
		err = fmt.Errorf("invalid hash len: %v,%v", len(hash), hash)
		return
	}

	//从迅雷种子库查找
	address := fmt.Sprintf(Xunlei, hash[:2], hash[len(hash)-2:], hash)
	req, err := http.NewRequest("GET", address, nil)
	if err != nil {
		return
	}
	req.Header.Set("User-Agent", "Mozilla/5.0")
	resp, err := client.Do(req)
	if err != nil {
		return
	}
	if resp != nil {
		defer func() {
			// io.Copy(ioutil.Discard, resp.Body)
			resp.Body.Close()
		}()

		if resp.StatusCode == 200 {
			//解析种子
			err = mi.Parse(resp.Body)
		} else if resp.StatusCode == 404 {
			err = ErrNotFound
		} else {
			err = errors.New("refuse error")
		}
	}
	return
}

func DownloadTorrage(hash string, client *http.Client) (mi MetaInfo, err error) {
	mi.InfoHash = hash

	hash = strings.ToUpper(hash)
	if len(hash) != 40 {
		err = fmt.Errorf("invalid hash len: %v,%v", len(hash), hash)
		return
	}

	//从迅雷种子库查找
	address := fmt.Sprintf(Torrage, hash)
	req, err := http.NewRequest("GET", address, nil)
	if err != nil {
		return
	}
	req.Header.Set("User-Agent", "Mozilla/5.0")
	resp, err := client.Do(req)
	if err != nil {
		return
	}
	utils.Log().Println(address)

	if resp != nil {
		defer func() {
			// io.Copy(ioutil.Discard, resp.Body)
			resp.Body.Close()
		}()

		if resp.StatusCode == 200 {
			//解析种子
			err = mi.Parse(resp.Body)
		} else if resp.StatusCode == 404 {
			err = ErrNotFound
		} else {
			err = fmt.Errorf("refuse error:%v", resp.StatusCode)
		}
	}
	return
}

func DownloadVuze(hash string, client *http.Client) (mi MetaInfo, err error) {
	mi.InfoHash = hash
	if len(hash) != 40 {
		err = fmt.Errorf("invalid hash len: %v,%v", len(hash), hash)
		return
	}
	sign := translate(hash)
	if sign == "" {
		err = errors.New("invalid sign len")
		return
	}

	address := fmt.Sprintf(Vuze, sign)
	req, err := http.NewRequest("GET", address, nil)
	if err != nil {
		return
	}
	req.Header.Set("User-Agent", "Azureus 5.7.5.0;Mac OS X;Java 1.8.0_121")
	req.Header.Set("Connection", "close")
	req.Header.Set("Host", "magnet.vuze.com")

	resp, err := client.Do(req)
	if err != nil {
		return
	}

	if resp != nil {
		defer func() {
			// io.Copy(ioutil.Discard, resp.Body)
			resp.Body.Close()
		}()

		if resp.StatusCode == 200 {
			//解析种子
			err = mi.Parse(resp.Body)
		} else if resp.StatusCode == 404 {
			err = ErrNotFound
		} else {
			err = fmt.Errorf("refuse error:%v", resp.StatusCode)
		}
	}
	return
}

func translate(str string) string {
	hs, _ := toBytes(str)

	buf := new(bytes.Buffer)
	for i := 0; i < len(hs); i += 2 {
		item := hs[i]*16 + hs[i+1]
		err := binary.Write(buf, binary.LittleEndian, item)
		if err != nil {
			fmt.Println("binary.Write failed:", err)
			return ""
		}
	}

	return base32.StdEncoding.EncodeToString(buf.Bytes())
}

func toBytes(str string) ([]byte, error) {
	bytes := make([]byte, len(str))
	for i := 0; i < len(str); i++ {
		switch {
		case str[i] >= '0' && str[i] <= '9':
			bytes[i] = str[i] - '0'
		case str[i] >= 'a' && str[i] <= 'z':
			bytes[i] = str[i] - 'a' + 10
		case str[i] >= 'A' && str[i] <= 'Z':
			bytes[i] = str[i] - 'A' + 10
		default:
			panic("")
		}
	}
	return bytes, nil
}

func pretty(v interface{}) {
	b, _ := json.MarshalIndent(v, " ", "  ")
	fmt.Println(string(b))
}
