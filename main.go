package main

import (
	"gitlab.com/btlike/storage/crawl"
	"gitlab.com/btlike/storage/utils"
)

func main() {
	utils.Init()
	crawl.Run()
}
