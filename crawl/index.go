package crawl

import (
	"time"

	"gitlab.com/btlike/storage/parser"
	"gitlab.com/btlike/storage/utils"
)

type torrentSearch struct {
	Name       string
	Length     int64
	FileCount  int64
	Heat       int64
	Infohash   string
	Files      []file
	CreateTime time.Time
}

type file struct {
	Name   string
	Length int64
}

func addToES(metaInfo *parser.MetaInfo, ipStr string) (err error) {
	if metaInfo.InfoHash == "" ||
		metaInfo.Info.Name == "" ||
		metaInfo.Info.Length == 0 {
		return
	}

	data := torrentSearch{
		Name:       metaInfo.Info.Name,
		Length:     metaInfo.Info.Length,
		CreateTime: time.Now(),
	}

	if len(metaInfo.Info.Files) == 0 {
		data.Length = metaInfo.Info.Length
		data.FileCount = 1
		data.Files = append(data.Files, file{Name: metaInfo.Info.Name, Length: metaInfo.Info.Length})
	} else {
		for _, v := range metaInfo.Info.Files {
			if len(v.Path) > 0 {
				data.Length += v.Length
				data.FileCount++
				data.Files = append(data.Files, file{
					Name:   v.Path[0],
					Length: v.Length,
				})
			}
		}
	}
	utils.Log().Printf("%#v\n", data)

	_, err = utils.Config.ES.Index().
		Index("torrent").
		Type("infohash").
		Id(metaInfo.InfoHash).
		BodyJson(data).
		Refresh(false).
		Do()
	return
}
